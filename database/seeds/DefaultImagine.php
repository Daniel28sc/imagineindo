<?php

use Illuminate\Database\Seeder;
use App\Imagine;

class DefaultImagine extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imagine = [
            [
            	'main_title' => 'BUILD YOUR BRAND
WITH US',
				'main_image' => 'images/home%20compres.png',
				'aboutUs_title' => 'About Us',
				'aboutUs_text' => '

Di Imagine para ahli pada bidangnya berkumpul dan berdiskusi untuk menghasilkan pekerjaan yang terbaik untuk klien. Kami sudah mempunyai pengalaman yang cukup pada bidangnya masing-masing. Kami juga peduli dengan bisnis anda karena keberhasilan anda juga keberhasilan kami.

"If your business is not on the internet, then your business will be out of business."

Bill Gates
',
				'aboutUs_image' => 'images/about%20compress.png',
				'case_title' => 'Our Case Study',
				'case_text' => 'Get to Know Us More Through Our Portfolio',
				'workflow_title' => 'Our Workflow',
			],
        ];

        foreach($imagine as $im)	{
    		Imagine::create($im);
		}
    }
}
