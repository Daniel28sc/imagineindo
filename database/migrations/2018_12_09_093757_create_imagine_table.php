<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagine', function (Blueprint $table) {
            $table->increments('id');
            // main
            $table->string('main_title')->nullable();
            $table->string('main_image')->nullable();
            // about us
            $table->string('aboutUs_title')->nullable();
            $table->text('aboutUs_text')->nullable();
            $table->string('aboutUs_image')->nullable();
            // case study
            $table->string('case_title')->nullable();
            $table->string('case_text')->nullable();
            // workflow
            $table->string('workflow_title')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagine');
    }
}
