<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// // });

// Route::get('/', function () {
//     return view('homepage');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('homepage');

Route::get('/', 'CmsController@showDefault');
Route::get('/edit', 'CmsController@showEdit');
Route::get('/uploadImage','CmsController@showAllImage');
Route::post('/uploadBranding','CmsController@uploadImageBranding');
Route::post('/edit','CmsController@saveEdit');
Route::post('/cancelEdit','CmsController@cancelEdit');
Route::post('/confrimEdit','CmsController@confrimEdit');

Route::get('/loginpage', 'UserController@showLoginPage');
Route::get('/registerpage', 'UserController@showRegisterPage');
Route::post('/signup', 'UserController@signup');
Route::get('/myprofil', 'UserController@myprofil');
Route::get('/logout', 'Auth\LoginController@logout');
