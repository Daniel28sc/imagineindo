@extends('layouts.hometemplate')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12" style="margin-top: 5%; margin-left: 10%;">
			<form method="post" action="{{url('edit')}}" enctype="multipart/form-data">
                @csrf
				<b><p>MAIN</p></b>
				<div class="form-group">
					<label>Main Title </label>
                    <input type="text" name="mainTitle" class="form-control" placeholder="" value="{{ $default->main_title }}">
                </div>
                <div class="form-group">
                    <label>Main Image </label>
                    <input type="file" name="mainImage" class="form-control">
                </div>
                <b><p>ABOUT US</p></b>
                <div class="form-group">
					<label>About Us Title</label>
                    <input type="text" name="aboutTitle" class="form-control" placeholder="" value="{{ $default->aboutUs_title }}">
                </div>
                <div class="form-group">
					<label>About Us Sentences</label>
                    <textarea id="aboutSentences" name = "aboutSentences" rows="5" cols="50" onKeyPress class="form-control">
                        {{ $default->aboutUs_text }}
                    </textarea>
                </div>
                <div class="form-group">
                    <label>About Us </label>
                    <input type="file" name="aboutImage" class="form-control">
                </div>
                <b><p>CASE STUDY</p></b>
                <div class="form-group">
					<label>Case Title</label>
                    <input type="text" name="caseTitle" class="form-control" placeholder="" value="{{ $default->case_title }}">
                </div>
                <div class="form-group">
					<label>Case Text</label>
                    <textarea id="caseSentences" name = "caseSentences" rows="5" cols="50" onKeyPress class="form-control">
                        {{ $default->case_text }}
                    </textarea>
                </div>
                <b><p>WORKFLOW</p></b>
                <div class="form-group">
					<label>Workflow Title</label>
                    <input type="text" name="wfTitle" class="form-control" placeholder="" value="{{ $default->workflow_title }}">
                </div>
                <button class="btn btn-danger" type="submit">SAVE</button>
			</form>
		</div>
	</div>
</div>
@endsection