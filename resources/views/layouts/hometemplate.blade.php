<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}">

    <title>Imagineindo.com</title>
    @include('includes.css')
    @include('includes.javascript')
</head>
<body>
    @include('includes.homeheader')
    @yield('content')
    @include('includes.homefooter')
</body>
</html>