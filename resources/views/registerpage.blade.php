@extends('layouts.hometemplate')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12" style="margin-top: 5%;">
				<p><b>REGISTER</b></p>
				<form id="register" method="POST" action="/signup">
					@csrf
					<div class="form-group">
						<label>Nama</label>
	                    <input type="text" name="name" class="form-control">
	                </div>
					<div class="form-group">
						<label>Email</label>
	                    <input type="email" name="email" class="form-control">
	                </div>
	                <div class="form-group">
						<label>Password</label>
	                    <input type="password" name="password" class="form-control">
	                </div>
	                <div class="form-group">
						<label>Confirm Password</label>
	                    <input type="password" name="confirmpassword" class="form-control">
	                </div>
	                <button class="btn btn-danger" type="submit">REGISTER</button>
				</form>
			</div>
		</div>
	</div>
@endsection