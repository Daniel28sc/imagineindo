<header class="header active">
        <nav class="navbar navbar-expand-lg sticky-top navbar-light">
                <a class="navbar-brand" href="#">
                    <img src="{{URL::asset('images/logo-header-gki.png')}}" width="150" height="auto" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse col-md-12 col-lg-12 offset-lg-4 offset-xl-6" style="padding:0;" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item nav-item2">
                      <form  action="{{url('confrimEdit')}}" method="post" accept-charset="utf-8">
                         @csrf
                        <button class="btn btn-danger" type="submit"> Save ?</button>
                      </form>
                    </li>
                    <li class="nav-item">
                      <form  action="{{url('cancelEdit')}}" method="post" accept-charset="utf-8">
                        <button class="btn btn-primary" type="submit"> Cancel</button>
                         @csrf
                      </form>
                    </li>
                  </ul>
                </div>
              </nav>
    </header>