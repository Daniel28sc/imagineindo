<footer class="container-fluid">
        <div class="row text-center align-items-center">
            <div class="col-sm-12 col-lg-4">
                <a href="" style="margin-right: 30px;color:white"><span class="footer-menu2">Home</a></span>
                <a href="" style="margin-right: 30px;color:white"><span class="footer-menu">About</a></span>
                <a href="" style="margin-right: 30px;color:white"><span class="footer-menu">Case Study</a></span>
                <a href="" style="color:white;"><span class="footer-menu">Contact Us</a></span>
            </div>
            <div class="col-sm-12 col-lg-4 text-center">
                    <p class="findus"> Find Us On &nbsp;&nbsp;&nbsp;&nbsp; <span class="footer-menu"></span>
                        <a href="https://www.instagram.com/imagineindo.co/" target="_blank"><img src="{{URL::asset('images/whatsapp.png')}}" style="width:20px;"></a>&nbsp;&nbsp;&nbsp;
                        <a href="https://www.instagram.com/imagineindo.co/" target="_blank"><img src="{{URL::asset('images/instagram.png')}}" style="width:20px;"></a>&nbsp;&nbsp;&nbsp;
                        <a href="https://www.instagram.com/imagineindo.co/" target="_blank"><img src="{{URL::asset('images/facebook.png')}}" style="width:20px;"></a>&nbsp;&nbsp;&nbsp;
                    </p>
                </div>
            <div class="col-sm-12 col-lg-4">
                <p class="footer">Imagine Copyright 2018. Allright Reserved</p>
            </div>
        </div>
    </footer>