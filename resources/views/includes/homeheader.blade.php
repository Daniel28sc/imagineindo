<header class="header active">
        <nav class="navbar navbar-expand-lg sticky-top navbar-light">
                <a class="navbar-brand" href="/">
                    <img src="{{URL::asset('images/logo-header-gki.png')}}" width="150" height="auto" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse col-md-12 col-lg-12 offset-lg-4 offset-xl-6" style="padding:0;" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item nav-item2">
                      <a class="nav-link mobile" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/#about">About</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/#casestudy">Case Study</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/#contact">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </nav>
    </header>