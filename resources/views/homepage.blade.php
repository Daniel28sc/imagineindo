@extends('layouts.hometemplate')

@section('content')
<main>
	<div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12"><img src="{{ $default->main_image }}" class="img" alt="resposive image"></div>
            <div style="padding-left:0;">
            <!-- <h1 class="centered">BUILD YOUR BRAND <br><span style="font-weight: bolder">WITH US</span></h1> -->
            <h1 class="centered">{{ $default->main_title }}</h1>
            </div>
        </div>
    </div>

    <!--About us-->
    <div class="container-fluid" id="about">
        <h2 class="text-center">{{ $default->aboutUs_title }}</h2>
        <div class="row align-items-center" style="margin-top:20px;">
            <div class="col-12 col-sm-4 col-md-4 col-lg-6 col-xl-6" style="padding-left:0; padding-right:0;">
                <img src="{{ $default->aboutUs_image }}" class="img-responsive" alt="about" style="border-radius:10px;">
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-8 offset-sm-4 offset-md-5 offset-lg-4 boxs">
                <p class="text-responsive">{{ $default->aboutUs_text }}
                </p>
            </div>
        </div>
    </div>

    <!--Case Study-->
    <div class="container" id="casestudy">
        <h2 class="text-center">{{ $default->case_title }}</h2>
        <p class="text-center subtitle">{{ $default->case_text }}</p>
        <div class="row justify-content-center">
            @if(count($brandings)>0)
                @foreach($brandings as $branding)
                <div class="col-xs-6 col-md-6 col-lg-3 box2">
                    <img src="{{ $branding->branding_image}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">{{$branding->branding_text}}</p>
                </div>
                @endforeach
            @else
                <div class="col-xs-6 col-md-6 col-lg-3 box2">
                    <img src="{{URL::asset('images/logo-doublechekk.png')}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">DoubleChekk</p>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-3 box2">
                    <img src="{{URL::asset('images/logo-pentacles.png')}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">Pentacles</p>
                </div>
                <div class="col-md-6 col-lg-3 box2">
                    <img src="{{URL::asset('images/logo-umbraco.png')}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">USpecialist</p>
                </div>
                <div class="col-md-6 col-lg-3 box2">
                    <img src="{{URL::asset('images/well.png')}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">Wellmint</p>
                </div>
                <div class="w-100"></div>
                <div class="col-md-6 col-lg-3 box2">
                    <a href="https://www.ugm.org" target="_blank"><img src="{{URL::asset('images/mockup-ugm.png')}}" alt="logo" class="casestudyimg"></a>
                    <p class="branding">Website Design</p>
                    <p class="brandingtitle">UGM</p>
                </div>
                <div class="col-md-6 col-lg-3 box2">
                    <a href="https://www.chainelectric.com" target="_blank"><img src="{{URL::asset('images/mockup-chain-electric.png')}}" alt="logo" class="casestudyimg"></a>
                    <p class="branding">Website Design</p>
                    <p class="brandingtitle">Chain Electric</p>
                </div>
                <div class="col-md-6 col-lg-3 box2">
                    <a href="https://www.apexfacility.com" target="_blank"><img src="{{URL::asset('images/mockup-apex.png')}}" alt="logo" class="casestudyimg"></a>
                    <p class="branding">Website Design</p>
                    <p class="brandingtitle">Apex Facility</p>
                </div>
                <div class="col-md-6 col-lg-3 box2">
                    <a href="https://www.21astor.com" target="_blank"><img src="{{URL::asset('images/mockup-21-astor.png')}}" alt="logo" class="casestudyimg"></a>
                    <p class="branding">Website Design</p>
                    <p class="brandingtitle">21 Astor</p>
                </div>
            @endif
        </div>
    </div>

    <!--workflow-->
    <div class="container">
        <h2 class="text-center">{{ $default->workflow_title }}</h2>
        <div class="row text-center justify-content-center" style="margin-top:30px;">
            @if(count($workflows)>0)
                @foreach($workflows as $workflow)
                    <div class="col-sm-2">
                        <img src="{{ $workflow->workflow_image }}" id="icon">
                        <p class="workflow">{{$workflow->workflow_text}}</p>
                    </div>
                @endforeach
            @else
                <div class="col-sm-2">
                    <img src="{{URL::asset('images/brainstorming.svg')}}" id="icon">
                    <p class="workflow">Brainstorming</p>
                </div>
                <div class="col-sm-2">
                    <img src="{{URL::asset('images/design.svg')}}" id="icon">
                    <p class="workflow">Design</p>
                </div>
                <div class="col-sm-2">
                    <img src="{{URL::asset('images/web-development.svg')}}" id="icon">
                    <p class="workflow">Development</p>
                </div>
                <div class="col-sm-2">
                    <img src="{{URL::asset('images/on-time-submit.svg')}}" id="icon">
                    <p class="workflow">On Time Submit</p>
                </div>
            @endif
        </div>
    </div>

    <!--Contact Us-->
    <div class="container-fluid" id="contact">
        <h2 class="text-center">Contact Us</h2>
        <p class="text-center subtitle">Make your Brand Activation with Us</p>
        <div class="row text-center justify-content-center">
            <form action="mailto:dennypratama194@gmail.com?Subject=Info%20Imagine" method="POST" enctype="text/plain">
                <div class="form-group">
                    <input type="text" name="nama" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <textarea name="deskripsi" class="form-control" rows="5" placeholder="Description"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
    </div>
</main>
@endsection