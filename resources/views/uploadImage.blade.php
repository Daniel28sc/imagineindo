@extends('layouts.hometemplate')

@section('content')
<main>

    <!--Case Study-->
    <div class="container" id="casestudy">
        <div class="row justify-content-center">
            @if(count($brandings)>0)
                @foreach($brandings as $branding)
                <div class="col-xs-6 col-md-6 col-lg-3 box2">
                    <img src="{{ $branding->branding_image}}" alt="logo" class="casestudyimg">
                    <p class="branding">Branding</p>
                    <p class="brandingtitle">{{$branding->branding_text}}</p>
                </div>
                @endforeach
                <form action="{{ URL::to('/uploadBranding') }}" method="post" enctype="multipart/form-data">
                    <label>Branding Image:</label>
                    <input type="file" name="branding_image" id="branding_image">
                    <br>
                    <label> Branding Title : </label>
                    <input type="text" name="branding_text" id="branding_text">
                    <input type="submit" value="Upload" name="submit">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                </form>
            @else
                <div class="col-md-6 col-lg-3 box2">
                    <a href="https://www.21astor.com" target="_blank"><img src="{{URL::asset('images/mockup-21-astor.png')}}" alt="logo" class="casestudyimg"></a>
                    <p class="branding">Website Design</p>
                    <p class="brandingtitle">21 Astor</p>
                </div>
            @endif
        </div>
    </div>

    <!--workflow-->
    <div class="container">
        <div class="row text-center justify-content-center" style="margin-top:30px;">
            @if(count($workflows)>0)
                @foreach($workflows as $workflow)
                    <div class="col-sm-2">
                        <img src="{{ $workflow->workflow_image }}" id="icon">
                        <p class="workflow">{{$workflow->workflow_text}}</p>
                    </div>
                @endforeach
            @else
                <div class="col-sm-2">
                    <img src="{{URL::asset('images/on-time-submit.svg')}}" id="icon">
                    <p class="workflow">On Time Submit</p>
                </div>
            @endif
        </div>
    </div>

</main>
@endsection