@extends('layouts.hometemplate')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12" style="margin-top: 5%;">
				<p><b>LOGIN</b></p>
				<form id="login" method="POST" action="{{route('login')}}">
					@csrf
					<div class="form-group">
						<label>Email</label>
	                    <input type="email" name="email" class="form-control">
	                </div>
	                <div class="form-group">
						<label>Password</label>
	                    <input type="password" name="password" class="form-control">
	                </div>
	                <button class="btn btn-danger" type="submit">LOGIN</button>
	                <p>Belum punya akun?<a href="/registerpage">Register</a></p>
				</form>
			</div>
		</div>
	</div>
@endsection