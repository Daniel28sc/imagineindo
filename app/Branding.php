<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branding extends Model
{
    protected $table = 'branding';
    protected $fillable = [
        'branding_text', 'branding_image',
    ];

    public $timestamp = true;
}
