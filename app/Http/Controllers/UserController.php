<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showLoginPage() {
    	return view('loginpage');
    }

    public function showRegisterPage() {
    	return view('registerpage');
    }

    use RegistersUsers;

    public function signup(Request $request) {
    	$rules = array(
            'name' => 'required|min:3|max:50',
            'email' => 'email',
            'password' => 'min:6|required_with:confirmpassword|same:confirmpassword'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $checkUser = User::where('email', $request->email)->first();

        if (!$checkUser) {
            $signup = new User();
            $signup->name = $request->name;
            $signup->email = $request->email;
            $signup->password = Hash::make($request->password);
            $signup->save();

            if ($signup) {
                Auth::login($signup, true);

                return redirect('myprofil');
            }

        } else {
            return redirect()->back()
                        ->withInput();
        }
    }

    public function myprofil() {
    	if (Auth::guest()) {
            return redirect('/');
        }
        $user = Auth::id();

        $saya = User::where('id', $user)->first();
        return view('myprofil', ['saya' => $saya]);
    }
}
