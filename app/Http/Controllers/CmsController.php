<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagine;
use App\Workflow;
use App\Branding;
use Auth;
use App\User;

class CmsController extends Controller
{
	public function showDefault() {
	    $defaults  = Imagine::all()->last();
	    $workflows = Workflow::all();
	    $brandings = Branding::all();
	    // dd($df);
	    return view('homepage', ['default' => $defaults, 'workflows' => $workflows, 'brandings' => $brandings]);
	}
	public function showAllImage() {
	    $workflows = Workflow::all();
	    $brandings = Branding::all();
	    // dd($df);
	    return view('uploadImage', ['workflows' => $workflows, 'brandings' => $brandings]);
	}

	public function uploadImageBranding(Request $request){
		$image = $request->file('branding_image');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/branding');
        $image->move($destinationPath, $name);
      
	 	$newBranding	= new Branding();
		$newBranding->branding_image 	= "images/branding/".$name;
	 	$newBranding->branding_text		= $request->branding_text;
	 	$newBranding->save();
	 	return response()->json([
	 		'success' => "ok berhasil"
	 	]);
	}

	public function showEdit(){
		if (Auth::guest()) {
            return redirect('/');
        }
		$defaults	= Imagine::all()->last();
		// dd($defaultIm);
		return view('editImagine',['default' => $defaults]);
	}

	public function saveEdit(Request $request){
		$oldIm	= Imagine::all()->last();
		$newIm	= $oldIm;
		$newIm->id	= $oldIm->id + 1;
		$newIm->main_title			= $request->mainTitle;
		$newIm->aboutUs_title		= $request->aboutTitle;
		$newIm->aboutUs_text		= $request->aboutSentences;
		$newIm->case_title 			= $request->caseTitle;
		$newIm->case_text 			= $request->caseSentences;
		$newIm->workflow_title		= $request->wfTitle;  
		$id = Imagine::create($newIm->toArray()); // mungkin nanti perlu 1 field, buat penanda, ini versi e aktif (udah di confrim) / belum;
		return view('/preview',['default' => $id]);
	}

	public function cancelEdit(){
		$defaultIm	= Imagine::all()->last();
		// dd($defaultIm);
		$defaultIm->delete();
		return redirect('edit'); // nanti kasi notif, kalau udh rollback, balik ke awal (cancel);
	}
	public function confrimEdit(){
		return redirect('/');
	}
}
