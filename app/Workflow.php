<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
    protected $table = 'workflow';
    protected $fillable = [
        'workflow_text', 'workflow_image',
    ];

    public $timestamp = true;
}
