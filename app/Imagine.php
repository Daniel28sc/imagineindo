<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagine extends Model
{
    protected $table = 'imagine';
    protected $fillable = [
        'main_title', 'main_image', 'aboutUs_title', 'aboutUs_text', 'aboutUs_image', 'case_title', 'case_text', 'workflow_title',
    ];

    public $timestamp = true;
}
